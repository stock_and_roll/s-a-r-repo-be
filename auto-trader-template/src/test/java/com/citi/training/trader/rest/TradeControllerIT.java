package com.citi.training.trader.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Trade;

/**
 * Integration test for Trade REST Interface.
 * <p>
 * Makes HTTP requests to {@link com.citi.training.trader.rest.TradeController}.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"h2", "no-scheduled"})
public class TradeControllerIT {

    private static final Logger logger =
            LoggerFactory.getLogger(TradeControllerIT.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.trading.trader.rest.trade-base-path:/trade}")
    private String tradeBasePath;

    @Test
    @Transactional
    public void findAll_returnsList() {
        Stock testStock = new Stock(-1, "AAPL");
        SimpleStrategy strategy = new SimpleStrategy(99, testStock, 199, 20, 0, 0.0, 0.0, new Date());

        Trade testTrade = new Trade(-1,100.0, 100, Trade.TradeType.BUY, Trade.TradeState.FILLED,LocalDateTime.now(),true, strategy);
        restTemplate.postForEntity(tradeBasePath,
                testTrade, Trade.class);

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                tradeBasePath,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Trade>>() {
                });

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(findAllResponse.getBody().get(0).getPrice(), testTrade.getPrice(), 0.0001);
    }

    @Test
    @Transactional
    public void findById_returnsCorrectId() {
        Stock testStock = new Stock(1, "AAPL");
        SimpleStrategy strategy = new SimpleStrategy(99, testStock, 199, 20, 0, 0.0, 0.0, new Date());
        Trade testTrade = new Trade(-1,100.0, 100, Trade.TradeType.BUY, Trade.TradeState.FILLED,LocalDateTime.now(),true, strategy);
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                        testTrade, Trade.class);

        assertEquals(HttpStatus.OK,createdResponse.getStatusCode());

        Trade foundTrade = restTemplate.getForObject(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                Trade.class);

        assertEquals(foundTrade.getId(), createdResponse.getBody().getId());
        assertEquals(foundTrade.getPrice(), testTrade.getPrice());
    }

    @Test
    @Transactional
    public void deleteById_deletes() {
        Stock testStock = new Stock(1, "AAPL");
        SimpleStrategy strategy = new SimpleStrategy(99, testStock, 199, 20, 0, 0.0, 0.0, new Date());
        Trade testTrade = new Trade(-1,100.0, 100, Trade.TradeType.BUY, Trade.TradeState.FILLED,LocalDateTime.now(),true, strategy);
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                        testTrade, Trade.class);

        assertEquals(createdResponse.getStatusCode(), HttpStatus.OK);

        Trade foundTrade = restTemplate.getForObject(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                Trade.class);

        logger.debug("Before delete, findById gives: " + foundTrade);
        assertNotNull(foundTrade);

        restTemplate.delete(tradeBasePath + "/" + createdResponse.getBody().getId());

        ResponseEntity<Trade> response = restTemplate.exchange(
                tradeBasePath + "/" + createdResponse.getBody().getId(),
                HttpMethod.GET,
                null,
                Trade.class);

        logger.debug("After delete, findById response code is: " +
                response.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
