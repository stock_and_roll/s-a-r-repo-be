package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;

/**
 * JDBC MySQL DAO implementation for trade table.
 *
 */
@Component
public class MysqlTradeDao implements TradeDao {

    private static final Logger logger =
                            LoggerFactory.getLogger(MysqlTradeDao.class);

    private static String FIND_ALL_SQL = "select trade.id as trade_id, strategy_id, price, " +
                                         "trade.size, buy, state, last_state_change, accounted_for, created, " +
            
                                         "stock_id, simple_strategy.size as strategy_size," +
                                         "exit_profit_loss, current_position, last_trade_price, " +
                                         "profit, stopped, " +

                                         "stock.ticker " +

                                         "from trade " +
                                         "left join simple_strategy on simple_strategy.id = trade.strategy_id " +
                                         "left join stock on stock.id = stock_id";

    private static String FIND_SQL = FIND_ALL_SQL + " where trade.id = ?";

    private static String FIND_BY_STATE_SQL = FIND_ALL_SQL + " where state = ?";

    private static String FIND_LATEST_BY_STRATEGY_SQL = FIND_ALL_SQL + " where strategy_id = ? order by -last_state_change limit 1";

    private static String INSERT_SQL = "INSERT INTO trade (strategy_id, price, size, buy, state, last_state_change, " +
                                       "accounted_for,created) " +
                                       "values (:strategy_id, :price, :size, :buy, :state, :last_state_change, " +
                                       ":accounted_for, :created)";

    private static String UPDATE_SQL = "UPDATE trade SET strategy_id=:strategy_id, price=:price, size=:size, " +
                                       "buy=:buy, state=:state, last_state_change=:last_state_change, " +
                                       "accounted_for=:accounted_for WHERE id=:id";

    private static String DELETE_SQL = "delete from trade where id=?";

    @Autowired
    private JdbcTemplate tpl;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Trade> findAll(){
        logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new TradeMapper());
    }

    @Override
    public Trade findById(int id) {
        logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
        List<Trade> Trades = this.tpl.query(FIND_SQL,
                new Object[]{id},
                new TradeMapper()
        );
        if(Trades.size() <= 0) {
            String warnMsg = "Requested Trade not found, id: " + id;
            logger.warn(warnMsg);
            throw new TradeNotFoundException(warnMsg);
        }
        if(Trades.size() > 1) {
            logger.warn("Found more than one Trade with id: " + id);
        }
        return Trades.get(0);
    }

    @Override
    public List<Trade> findAllByState(Trade.TradeState state) {
        logger.debug("findAllByState(" + state + ") SQL: [" + FIND_BY_STATE_SQL + "]");
        List<Trade> Trades = this.tpl.query(FIND_BY_STATE_SQL,
                new Object[]{state.toString()},
                new TradeMapper()
        );
        return Trades;
    }

    @Override
    public Trade findLatestByStrategyId(int strategyId) {
        logger.debug("findLatestByStrategyId(" + strategyId + ") SQL: [" + FIND_LATEST_BY_STRATEGY_SQL + "]");
        List<Trade> trades = this.tpl.query(FIND_LATEST_BY_STRATEGY_SQL,
                new Object[]{strategyId},
                new TradeMapper()
        );

        if(trades.size() == 0) {
            throw new TradeNotFoundException("No trades found for strategy: " + strategyId);
        }
        return trades.get(0);
    }

    public int save(Trade trade) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("strategy_id", trade.getStrategy().getId());
        namedParameters.addValue("price", trade.getPrice());
        namedParameters.addValue("size", trade.getSize());
        namedParameters.addValue("buy", trade.getTradeType().equals(Trade.TradeType.BUY) ? 1 : 0);
        namedParameters.addValue("state", trade.getState().toString());
        namedParameters.addValue("last_state_change", trade.getLastStateChange());
        namedParameters.addValue("accounted_for", trade.getAccountedFor());

        if (trade.getId() < 0) {
            logger.debug("Inserting trade: " + trade);
            namedParameters.addValue("created", LocalDateTime.now());

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            trade.setId(keyHolder.getKey().intValue());
        } else {
            logger.debug("Updating trade: " + trade);
            namedParameters.addValue("id", trade.getId());
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
        }

        logger.debug("Saved trade: " + trade);
        return trade.getId();
    }

    @Override
    public void deleteById(int id) {
        logger.debug("deleteById(" + id + ") SQL: [" + DELETE_SQL +"]");
        if(this.tpl.update(DELETE_SQL, id) <= 0) {
            String warnMsg = "Failed to delete, trade not found: " + id;
            logger.warn(warnMsg);
            throw new TradeNotFoundException(warnMsg);
        }
        else {
            for(Trade trade: findAll()) {
                logger.debug(trade.toString());
            }
        }
    }

    /**
     * private internal class to map database rows to Trade objects.
     *
     */
    private static final class TradeMapper implements RowMapper<Trade> {
        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping trade result set row num [" + rowNum + "], id : [" +
                         rs.getInt("trade_id") + "]");

            Stock stock = new Stock(rs.getInt("stock_id"), rs.getString("ticker"));

            SimpleStrategy strategy = new SimpleStrategy(rs.getInt("strategy_id"),
                                                         stock,
                                                         rs.getInt("strategy_size"),
                                                         rs.getDouble("exit_profit_loss"),
                                                         rs.getInt("current_position"),
                                                         rs.getDouble("last_trade_price"),
                                                         rs.getDouble("profit"), rs.getDate("stopped"));

            return new Trade(rs.getInt("trade_id"),
                             rs.getDouble("price"),
                             rs.getInt("size"),
                             rs.getInt("buy") == 1 ? "BUY" : "SELL",
                             rs.getString("state"),
                             (LocalDateTime)rs.getObject("last_state_change", LocalDateTime.class),
                             rs.getBoolean("accounted_for"),
                             strategy);
        }
    }
}
