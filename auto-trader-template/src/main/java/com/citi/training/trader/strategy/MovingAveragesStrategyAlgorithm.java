package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.StrategyService;
import com.citi.training.trader.service.TradeService;

/**
 * This is the algorithm used to perform auto trading for the customer. It calculates two averages, one for the last
 * "LONG_AVERAGE_SIZE" of prices, and one for the last "SHORT_AVERAGE_SIZE"
 */


@Profile("!no-scheduled")
@Component
public class MovingAveragesStrategyAlgorithm implements StrategyAlgorithm {

    private static final Logger logger =
            LoggerFactory.getLogger(MovingAveragesStrategyAlgorithm.class);

    @Autowired
    private TradeSender tradeSender;

    @Autowired
    private PriceService priceService;

    @Autowired
    private StrategyService strategyService;

    @Autowired
    private TradeService tradeService;

    private static final int LONG_AVERAGE_SIZE = 60;

    private static final int SHORT_AVERAGE_SIZE = 15;

    private double oldShortAverage = 0;
    @Scheduled(fixedRateString = "${moving.averages.strategy.refresh.rate_ms:1000}")
    public void run() {

        //do this code for all strategies in the simple_strategies table that are not fulfilled
        for(SimpleStrategy strategy: strategyService.findAll()) {

            //initialise trade variable
            Trade lastTrade = null;
            try {
                //gets last trade for current strategy
                lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
            } catch(TradeNotFoundException ex) {
                logger.debug("No Trades for strategy id: " + strategy.getId()+", exit criteria may have been met.");
            }

            //if there is data for last trade, wait for it to finish before doing another one
            if(lastTrade != null) {
                if(lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
                    logger.debug("Waiting for last trade to complete, do nothing");
                    continue;
                }

                //if the last trade is filled but not accounted for, account for it
                if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
                    logger.debug("Accounting for Trade: " + lastTrade);
                    if(!strategy.hasPosition()) {
                        if(lastTrade.getTradeType() == Trade.TradeType.SELL) {
                            logger.debug("Confirmed short position for strategy: " + strategy);
                            strategy.takeShortPosition();
                        }
                        else {
                            logger.debug("Confirmed long position for strategy: " + strategy);
                            strategy.takeLongPosition();
                        }
                        strategy.setLastTradePrice(lastTrade.getPrice());
                    }
                    else if(strategy.hasLongPosition()) {
                        logger.debug("Closing long position for strategy: " + strategy);
                        logger.info("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
                                lastTrade.getPrice());
                        closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
                    }
                    else if(strategy.hasShortPosition()) {
                        logger.debug("Closing short position for strategy: " + strategy);
                        logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
                                lastTrade.getPrice());
                        closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
                    }
                }

                lastTrade.setAccountedFor(true);
                tradeService.save(lastTrade);
            }


            // TODO: We should be checking for open positions and
            // closing them if necessary
            if(strategy.getStopped() != null) {
                continue;
            }

            logger.debug("Taking strategic action");
                // we have no open position
                List<Price> prices = priceService.findLatest(strategy.getStock(), LONG_AVERAGE_SIZE);

                //don't do anything if there isn't enough history to make a judgement
                if(prices.size() < LONG_AVERAGE_SIZE) {
                    logger.warn("Not enough price history for: "+ strategy.getStock()+" to execute strategy, not enough " +
                            "price data yet: " +
                            strategy);
                    continue;
                }

                logger.debug("Taking position based on prices:");

                double averageLongPriceInitial=0;
                double averageShortPriceInitial=0;
                double sumLongPriceInitial=0;
                double sumShortPriceInitial=0;

                //gets latest prices from the original sized list
                List<Price> shortAveragePrices = prices.subList(0,SHORT_AVERAGE_SIZE);

                //long average
                for(Price price: prices) {
                    sumLongPriceInitial+=price.getPrice();
                }

                averageLongPriceInitial = sumLongPriceInitial/prices.size();

                //short average
                for(Price price:shortAveragePrices){
                    sumShortPriceInitial+=price.getPrice();
                }
                averageShortPriceInitial = sumShortPriceInitial/shortAveragePrices.size();


                // if average price going down => take short => sell
                double averagePriceChange = averageShortPriceInitial - averageLongPriceInitial;
                logger.info(strategy.getId()+" :Current Average Price change: " + averagePriceChange);

                if(averagePriceChange < 0.001 && averagePriceChange > -0.001) {
                    logger.debug("Insufficient price change, taking no action");
                    continue;
                }

                if(oldShortAverage==0){
                    //first time running, take the initial short average and rerun
                    oldShortAverage = averageShortPriceInitial;
                    continue;

                } else if(!(strategy.getCurrentPosition()==-1)&&averageShortPriceInitial<(averageLongPriceInitial)&&oldShortAverage>averageLongPriceInitial) {
                    // if average price going down => take short => sell
                    logger.debug("Trading to open short position for strategy: " + strategy);
                    makeTrade(strategy, Trade.TradeType.SELL);
                } else if(!(strategy.getCurrentPosition()==1)&&averageShortPriceInitial>(averageLongPriceInitial)&&oldShortAverage<averageLongPriceInitial) {
                    // if average price going up => take long => buy
                    logger.debug("Trading to open long position for strategy: " + strategy);
                    makeTrade(strategy, Trade.TradeType.BUY);
                }else{
                    logger.info("Not a turning point, do nothing");
                }

                oldShortAverage = averageShortPriceInitial;


            strategyService.save(strategy);
        }
    }



    private void closePosition(double profitLoss, SimpleStrategy strategy) {
        logger.info("Recording profit/loss of: " + profitLoss +
                " for strategy: " + strategy);
        strategy.addProfitLoss(profitLoss);
        strategy.closePosition();

        if(strategy.getProfit() >= strategy.getExitProfitLoss()) {
            logger.info("Exit condition reached, exiting strategy");
            strategy.setStopped(new Date());
            strategyService.save(strategy);
        }
    }


    private double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(currentPrice.getPrice(),
                strategy.getSize(), tradeType,
                strategy));
        return currentPrice.getPrice();
    }

}
