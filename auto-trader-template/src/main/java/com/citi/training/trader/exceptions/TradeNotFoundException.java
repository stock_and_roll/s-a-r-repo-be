package com.citi.training.trader.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An exception to be thrown by {@link com.citi.training.trader.dao.TradeDao} implementations
 * when a requested stock is not found.
 *
 */
@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Trade not found")
public class TradeNotFoundException extends RuntimeException {
    public TradeNotFoundException(String msg) {
        super(msg);
    }
}
