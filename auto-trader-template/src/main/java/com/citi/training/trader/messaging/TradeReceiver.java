package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

/**
 * Service class that will listen on JMS queue for Trades and put
 * returned trades into the database.
 *
 */
@Component
public class TradeReceiver {

    private static final Logger logger =
                    LoggerFactory.getLogger(TradeReceiver.class);

    @Autowired
    private TradeService tradeService;

    @JmsListener(destination = "${jms.sender.responseQueueName:OrderBroker_Reply}")
    public void receiveTrade(String xmlReply) {

        logger.debug("Processing OrderBroker Reply:");
        logger.debug(xmlReply);

        Trade replyTrade = Trade.fromXml(xmlReply);
        Trade savedTrade;
        logger.debug("Parsed returned trade: " + replyTrade);

        try {
            savedTrade = tradeService.findById(replyTrade.getId());
        } catch(TradeNotFoundException ex) {
            logger.error("Trade reply received, but Trade not found, id: " + replyTrade.getId());
            return;
        }

        if (xmlReply.contains("<result>REJECTED</result>")) {
            savedTrade.stateChange(Trade.TradeState.REJECTED);
            logger.error("TRADE WAS REJECTED" +savedTrade.getId());
        } else if (xmlReply.contains("<result>Partially_Filled</result>")) {
            savedTrade.stateChange(Trade.TradeState.INIT);
            logger.error("TRADE WAS INITIALISED" +savedTrade.getId());
        } else {
            savedTrade.stateChange(Trade.TradeState.FILLED);
            logger.error("TRADE WAS FILLED" +savedTrade.getId());
        }
        tradeService.save(savedTrade);
    }
}
