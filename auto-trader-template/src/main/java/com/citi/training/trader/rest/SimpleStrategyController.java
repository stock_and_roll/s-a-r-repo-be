package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.service.StrategyService;
import com.citi.training.trader.model.SimpleStrategy;

/**
 * REST Controller for {@link com.citi.training.trader.model.SimpleStrategy} resource.
 *
 */
@RestController
@RequestMapping("${com.citi.training.trader.rest.simpleStrategy-base-path:/api/strategy}")
public class SimpleStrategyController {

    private static final Logger logger =
            LoggerFactory.getLogger(SimpleStrategyController.class);

    @Autowired
    private StrategyService StrategyService;

    @RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public List<SimpleStrategy> findAll() {
        logger.debug("findAll()");
        return StrategyService.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public SimpleStrategy findById(@PathVariable int id) {
        logger.debug("findById(" + id + ")");
        return StrategyService.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy simpleStrategy) {
        logger.debug("create(" + simpleStrategy + ")");

        simpleStrategy.setId(StrategyService.save(simpleStrategy));

        logger.debug("created simpleStrategy: " + simpleStrategy);

        return new ResponseEntity<SimpleStrategy>(simpleStrategy, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        logger.debug("deleteById(" + id + ")");
        StrategyService.deleteById(id);
    }
}
