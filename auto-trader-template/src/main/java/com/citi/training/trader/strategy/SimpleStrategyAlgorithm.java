//package com.citi.training.trader.strategy;
//
//import java.util.Date;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Profile;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import com.citi.training.trader.exceptions.TradeNotFoundException;
//import com.citi.training.trader.messaging.TradeSender;
//import com.citi.training.trader.model.Price;
//import com.citi.training.trader.model.SimpleStrategy;
//import com.citi.training.trader.model.Trade;
//import com.citi.training.trader.model.Trade.TradeState;
//import com.citi.training.trader.service.PriceService;
//import com.citi.training.trader.service.StrategyService;
//import com.citi.training.trader.service.TradeService;
//
////@Profile("!no-scheduled")
//@Component
//public class SimpleStrategyAlgorithm implements StrategyAlgorithm {
//
//    private static final Logger logger =
//                LoggerFactory.getLogger(SimpleStrategyAlgorithm.class);
//
//    @Autowired
//    private TradeSender tradeSender;
//
//    @Autowired
//    private PriceService priceService;
//
//    @Autowired
//    private StrategyService strategyService;
//
//    @Autowired
//    private TradeService tradeService;
//
//    @Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
//    public void run() {
//
//        for(SimpleStrategy strategy: strategyService.findAll()) {
//
//            Trade lastTrade = null;
//            try {
//                lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
//            } catch(TradeNotFoundException ex) {
//                logger.debug("No Trades for strategy id: " + strategy.getId());
//            }
//
//            if(lastTrade != null) {
//                if(lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
//                    logger.debug("Waiting for last trade to complete, do nothing");
//                    continue;
//                }
//
//                // account for last trade?
//                if(!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
//                    logger.debug("Accounting for Trade: " + lastTrade);
//                    if(!strategy.hasPosition()) {
//                        if(lastTrade.getTradeType() == Trade.TradeType.SELL) {
//                            logger.debug("Confirmed short position for strategy: " + strategy);
//                            strategy.takeShortPosition();
//                        }
//                        else {
//                            logger.debug("Confirmed long position for strategy: " + strategy);
//                            strategy.takeLongPosition();
//                        }
//                        strategy.setLastTradePrice(lastTrade.getPrice());
//                    }
//                    else if(strategy.hasLongPosition()) {
//                        logger.debug("Closing long position for strategy: " + strategy);
//                        logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " +
//                                     lastTrade.getPrice());
//                        closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
//                    }
//                    else if(strategy.hasShortPosition()) {
//                        logger.debug("Closing short position for strategy: " + strategy);
//                        logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " +
//                                     lastTrade.getPrice());
//                        closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
//                    }
//                }
//
//                lastTrade.setAccountedFor(true);
//                tradeService.save(lastTrade);
//            }
//
//
//            // TODO: We should be checking for open positions and
//            // closing them if necessary
//            if(strategy.getStopped() != null) {
//                continue;
//            }
//
//            logger.debug("Taking strategic action");
//            if(!strategy.hasPosition()) {
//                // we have no open position
//                List<Price> prices = priceService.findLatest(strategy.getStock(), 2);
//
//                if(prices.size() < 2) {
//                    logger.warn("Unable to execute strategy, not enough price data: " +
//                                strategy);
//                    continue;
//                }
//
//                logger.debug("Taking position based on prices:");
//                for(Price price: prices) {
//                    logger.debug(price.toString());
//                }
//
//                // if price going down => take short => sell
//                double currentPriceChange = prices.get(0).getPrice() - prices.get(1).getPrice();
//                logger.debug("Current Price change: " + currentPriceChange);
//
//                if(currentPriceChange < 0.001 && currentPriceChange > -0.001) {
//                    logger.debug("Insufficient price change, taking no action");
//                    continue;
//                }
//                if(currentPriceChange < 0) {
//                    logger.debug("Trading to open short position for strategy: " + strategy);
//                    makeTrade(strategy, Trade.TradeType.SELL);
//                } else {
//                    // if price going up => take long => buy
//                    logger.debug("Trading to open long position for strategy: " + strategy);
//                    makeTrade(strategy, Trade.TradeType.BUY);
//                }
//
//            } else if(strategy.hasLongPosition()) {
//                // we have a long position => close the position by selling
//                logger.debug("Trading to close long position for strategy: " + strategy);
//                makeTrade(strategy, Trade.TradeType.SELL);
//            } else if(strategy.hasShortPosition()) {
//                // we have a short position => close the position by buying
//                logger.debug("Trading to close short position for strategy: " + strategy);
//                makeTrade(strategy, Trade.TradeType.BUY);
//            }
//            strategyService.save(strategy);
//        }
//    }
//
//    private void closePosition(double profitLoss, SimpleStrategy strategy) {
//        logger.debug("Recording profit/loss of: " + profitLoss +
//                     " for strategy: " + strategy);
//        strategy.addProfitLoss(profitLoss);
//        strategy.closePosition();
//
//        if(strategy.getProfit() >= strategy.getExitProfitLoss()) {
//            logger.debug("Exit condition reached, exiting strategy");
//            strategy.setStopped(new Date());
//            strategyService.save(strategy);
//        }
//    }
//
//
//    private double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
//        Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
//        tradeSender.sendTrade(new Trade(currentPrice.getPrice(),
//                                        strategy.getSize(), tradeType,
//                                        strategy));
//        return currentPrice.getPrice();
//    }
//
//}
